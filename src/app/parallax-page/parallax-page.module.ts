import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParallaxPageComponent } from './parallax-page.component';

@NgModule({
  declarations: [
    ParallaxPageComponent
  ],
  imports: [
    CommonModule
  ],
  bootstrap:[ParallaxPageComponent]
})
export class ParallaxPageModule { }
