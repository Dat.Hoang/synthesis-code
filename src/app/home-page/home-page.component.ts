import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  public navBar:{
    name: string,
    url: string
    }[] = [{
      name: 'Home',
      url: '/'
    },{
      name: 'Parallax',
      url: '/parallax'
    },{
      name: 'Management',
      url: '/manage'
    }];
  public socialMedia:{
    name: string,
    url: string
  }[] = [{
    name: 'youtube',
    url: '#youtube'
  },{
    name: 'facebook-square',
    url: '#facebook'
  },{
    name: 'instagram',
    url: '#instagram'
  }];
  public backgrounds: string[] = ['Gradient', 'Picture'];

  public isFooterHide: boolean = false;
  public isBackgroundChooserHide: boolean = true;
  public backgroundType: string = 'Gradient';

  constructor() { }

  ngOnInit() {
  }

  toggleFooter(){
    this.isFooterHide = !this.isFooterHide;
  }

  toggleBackground(){
    this.isBackgroundChooserHide= !this.isBackgroundChooserHide;
  }

  changeBackgroundType(type: string){
    this.backgroundType = type;
    this.toggleBackground();
  }

}

