import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManagementComponent } from './management.component';
import { DragdropRequestComponent } from './dragdrop-request/dragdrop-request.component';


const routes: Routes = [
  {
    path: '',
    component: ManagementComponent,
    children:[{
        path: 'drag-drop',
        component: DragdropRequestComponent
    },
    {
        path: '**',
        redirectTo: 'drag-drop' 
    },
    ]
    
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagementRoutingModule { }