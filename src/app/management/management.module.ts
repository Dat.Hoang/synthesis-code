import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagementComponent } from './management.component';
import { ManagementRoutingModule } from './management.routing';
import { DragdropRequestComponent } from './dragdrop-request/dragdrop-request.component';
import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
  declarations: [
    ManagementComponent,
    DragdropRequestComponent
  ],
  imports: [
    CommonModule,
    ManagementRoutingModule,
    DragDropModule,
  ]
})
export class ManagementModule {
  
}
