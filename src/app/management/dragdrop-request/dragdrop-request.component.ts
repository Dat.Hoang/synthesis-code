import { Component, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-dragdrop-request',
  templateUrl: './dragdrop-request.component.html',
  styleUrls: ['./dragdrop-request.component.scss']
})
export class DragdropRequestComponent implements OnInit {
  public fromList:string[] = [
    ...("something nice is happen".split(" "))
  ];

  public toList:string[] = [
    ...("oke done".split(" "))
  ];

  constructor() { 

  }

  ngOnInit() {
  }


  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.fromList, event.previousIndex, event.currentIndex);
  }

  // drop(event: CdkDragDrop<string[]>) {
  //   if (event.previousContainer === event.container) {
  //     moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
  //   } else {
  //     transferArrayItem(event.previousContainer.data,
  //                       event.container.data,
  //                       event.previousIndex,
  //                       event.currentIndex);
  //   }
  // }

}
