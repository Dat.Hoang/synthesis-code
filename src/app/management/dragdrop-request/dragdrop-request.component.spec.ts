import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DragdropRequestComponent } from './dragdrop-request.component';

describe('DragdropRequestComponent', () => {
  let component: DragdropRequestComponent;
  let fixture: ComponentFixture<DragdropRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DragdropRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DragdropRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
